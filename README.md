# Vue Project Boilerplate

Boilerplate para desenvolvimento de projetos utilizando Vue, Vuetify e Capacitor.
Esse projeto ainda está em desenvolvimento.

_Principais Features:_

- Gerenciamento de navegação, menus e módulos via API.
- Renderização de componentes externos.
- Setup preparado para build de apps hibridos multi plataforma, além de PWA.
- Gerência de cache por SW.

_TODO_

- Push notifications (possui bug no iOs).
- Utilizar DB local com sync em background.

## Setup

### Termos padronizados para substituição

Utilize a ferramenta "Find and Replace" para encontrar e substituir os termos ´´´boilerplate´´´ e ´´´%REPLACE%´´´.

### Mocked API

A Api de mocks se inicia automaticamente junto com o ambiente de desenvolvimento. Alterações nos dados mockados não atualizadas automaticamente.

_TODO_

- Hot reload na API de Mocks.

### Prettier:

Instale a Extensão Prettier (esbenp.prettier-vscode)
Acesse as configurações do seu VSCode pelo comando `CTRL + SHIFT + P` e digite `Open Settings (JSON)` e no arquivo que será aberto adicione as seguintes configurações:

```
{
  "remote.WSL.fileWatcher.polling": true,
  "[vue, javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnSave": true
  },
  "editor.formatOnSave": true,
  "eslint.autoFixOnSave": true
}
```

## Scripts

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
