import Rest from '@/_Tools/Rest'

/**
 * @typedef {AppServices}
 */
export default class AppServices extends Rest {
  /**
   * @type {String}
   */
  static resource = '/'
}
