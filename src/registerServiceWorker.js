/* eslint-disable no-console */

import { register } from 'register-service-worker'

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV) {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready(a, b) {
      console.log(a, b)
      console.log(`App is being served from cache by a service worker.\n
        For more details, visit https://goo.gl/AFskqB`)
    },
    registered(a, b) {
      console.log(a, b)
      console.log('Service worker has been registered.')
    },
    cached(a, b) {
      console.log(a, b)
      console.log('Content has been cached for offline use.')
    },
    updatefound(a, b) {
      console.log(a, b)
      console.log('New content is downloading.')
    },
    updated(a, b) {
      console.log(a, b)
      console.log('New content is available; please refresh.')
      // alert('Conteudo foi atualizado. A página será recarregada.')
      // window.location.reload(true)
    },
    offline(a, b) {
      console.log(a, b)
      console.log(`No internet connection found.
        App is running in offline mode.`)
    },
    error(error, a, b) {
      console.log(a, b)
      console.error('Error during service worker registration:', error)
    },
  })
}
