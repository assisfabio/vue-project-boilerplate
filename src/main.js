import Vue from 'vue'
import * as firebase from 'firebase/app'
import 'firebase/analytics'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

var firebaseConfig = {
  apiKey: '%REPLACE%',
  authDomain: 'boilerplate.firebaseapp.com',
  databaseURL: 'https://boilerplate.firebaseio.com',
  projectId: 'boilerplate',
  storageBucket: 'boilerplate.appspot.com',
  messagingSenderId: '%REPLACE%',
  appId: '%REPLACE%',
  measurementId: '%REPLACE%',
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)
firebase.analytics()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
