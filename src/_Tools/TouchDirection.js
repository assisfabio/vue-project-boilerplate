export default class TouchDirection {
  static element = null

  constructor(el, action) {
    this.element = el
    this.element.touchActions = action
  }

  build() {
    console.log('build')
    this.element.addEventListener('touchstart', this.handleEvent, false)
    this.element.addEventListener('touchend', this.handleEvent, false)
    this.element.addEventListener('touchcancel', this.handleEvent, false)
    this.element.addEventListener('touchleave', this.handleEvent, false)
    this.element.addEventListener('touchmove', this.handleEvent, false)
  }

  /**
   * HandleEvent function are associated with target HTMLElement
   * So: this Class = this.element (¬¨¬)
   * @param {*} e touchEvent
   */
  handleEvent(e) {
    const directions = {}
    this.touchStart = this.touchStart || {}
    this.touchEnd = this.touchEnd || {}
    switch (e.type) {
      case 'touchstart':
        this.touchStart.y = e.touches && e.touches[0].clientY
        this.touchStart.x = e.touches && e.touches[0].clientX
        break

      case 'touchmove':
        this.touchEnd.y = e.touches && e.touches[0].clientY
        this.touchEnd.x = e.touches && e.touches[0].clientX
        break

      case 'touchend':
        if (e.touches && e.touches[0]) {
          this.touchEnd.y = e.touches[0].clientY
          this.touchEnd.x = e.touches[0].clientX
        }

        directions.v = this.touchStart.y - this.touchEnd.y >= 0 ? 'up' : 'down'
        directions.h =
          this.touchStart.x - this.touchEnd.x >= 0 ? 'left' : 'right'
        directions.start = this.touchStart
        directions.end = this.touchEnd
        directions.distance = {
          x: Math.abs(this.touchStart.x - this.touchEnd.x),
          y: Math.abs(this.touchStart.y - this.touchEnd.y),
        }

        directions.selected =
          directions.distance.y > directions.distance.x
            ? directions.v
            : directions.h

        if (this.touchActions) {
          this.touchActions(directions)
          this.touchStart = {}
          this.touchEnd = {}
        }
        break

      default:
        break
    }
  }
}
