export default class ColorScheme {
  static $vuetify
  static ignoreOsScheme
  static userColorScheme

  constructor($vuetify) {
    this.$vuetify = $vuetify
    this.ignoreOsScheme = !!localStorage.getItem('ignore-os-color-scheme')
    this.userColorScheme =
      localStorage.getItem('prefers-color-scheme') || 'light'
  }

  getIgnoreOsScheme() {
    return this.ignoreOsScheme
  }

  getUserColorScheme() {
    return this.userColorScheme
  }

  setUserColorScheme(value) {
    this.userColorScheme = value
    localStorage.setItem('prefers-color-scheme', value)
    if (this.getIgnoreOsScheme()) {
      this.$vuetify.theme.dark = value === 'dark'
    }
  }

  getOsScheme() {
    return window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)')?.matches
      ? 'dark'
      : 'light'
  }

  getOsSchemeIsDark() {
    return (
      window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: dark)')?.matches
    )
  }

  bypassOsScheme() {
    this.ignoreOsScheme = true
    localStorage.setItem('ignore-os-color-scheme', true)
    // FIXME $vuetify set propertie
    this.$vuetify.theme.dark = this.getUserColorScheme() === 'dark'
  }

  useOsScheme() {
    this.ignoreOsScheme = false
    localStorage.removeItem('ignore-os-color-scheme')
    // FIXME $vuetify set propertie
    this.$vuetify.theme.dark = this.getOsSchemeIsDark()
  }
}
