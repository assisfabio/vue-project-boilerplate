import Loading from '@/components/LoadingComponent'
// import ExternalComponent from './ExternalComponent'

export async function ExternalComponent(component) {
  const url = component.src
  const name = url
    .split('/')
    .reverse()[0]
    .match(/^(.*?)\.umd/)[1]

  if (window[name]) return window[name]

  // Appending CSS
  if (component.style) {
    const link = document.createElement('link')
    link.rel = 'stylesheet'
    link.href = component.style
    document.head.appendChild(link)
  }

  window[name] = new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.async = true
    script.addEventListener('load', () => {
      resolve(window[name])
    })
    script.addEventListener('error', () => {
      reject(new Error(`Error loading ${url}`))
    })
    script.src = url
    document.head.appendChild(script)
  })

  return window[name]
}

export default component => ({
  // O componente a carregar (deve ser uma Promise)
  component: ExternalComponent(component),
  // Um componente para usar enquanto o assíncrono é carregado
  loading: Loading,
  // Um componente para usar se o carregamento falhar
  error: Loading,
  // Espera antes de exibir o componente de loading. Padrão: 200ms
  delay: 200,
  // O componente de erro será exibido se um timemout for
  // fornecido e excedido. Padrão: Infinity
  timeout: 30000,
})
