import axios from 'axios'
import store from '@/store'

const standard = axios.create({
  baseURL: '/',
  validateStatus: status => status >= 200 && status < 300,
  timeout: 100000,
  transformResponse: [data => data],
})

standard.interceptors.request.use(
  async config => {
    console.log('INCREMENT LOADING ON:REQUEST', store.state.loading)
    store.commit('loadingIncrement') // loadingDecrement
    /*
    if (
      !config.url.endsWith('login') ||
      !config.url.endsWith('refresh') ||
      !config.url.endsWith('signup')
    ) {
      const userTokenExpiration = new Date(await AsyncStorage.getItem('userTokenExpiration'));
      const today = new Date();
      if (today > userTokenExpiration) {
        // refresh the token here
        const userRefreshToken = await AsyncStorage.getItem('userRefreshToken');
      } else {
        const userToken = await AsyncStorage.getItem('userToken');
        config.headers.Authorization = `Bearer ${userToken}`;
      }
    }
    */
    return config
  },
  error => {
    console.log('INCREMENT LOADING ON:REQUEST ERROR')
    return Promise.reject(error)
  }
)

standard.interceptors.response.use(
  response => {
    console.log('DECREASE LOADING ON:RESPONSE SUCCESS')
    store.commit('loadingDecrement')
    if (!response.data) {
      return response
    }
    if (typeof response.data === 'string') {
      return JSON.parse(response.data)
    }
    return response.data
  },
  error => {
    console.log('DECREASE LOADING ON:RESPONSE ERROR')
    store.commit('loadingDecrement')
    let result
    if (!error.response || !error.response.data) {
      result = error.message || error
    }
    if (error.response.data) {
      if (typeof error.response.data === 'string') {
        result = JSON.parse(error.response.data)
      } else {
        result = error.response.data
      }
    }
    result = result || error.response
    return Promise.reject(result)
  }
)

export default standard
