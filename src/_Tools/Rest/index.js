import Api from './Api'
import Http from './Http'
import Rest from './Rest'
import Service from './Service'
import Standard from './Standard'

export default Rest

export { Api, Http, Rest, Service, Standard }
