import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import pt from 'vuetify/es5/locale/pt'

import ColorScheme from '@/_Tools/ColorScheme'

const colorScheme = new ColorScheme()

Vue.use(Vuetify)

let darkMode = false
if (colorScheme.getIgnoreOsScheme()) {
  darkMode = colorScheme.getUserColorScheme() === 'dark'
} else {
  darkMode = colorScheme.getOsSchemeIsDark()
}

export default new Vuetify({
  theme: {
    dark: darkMode,
    options: {
      customProperties: true,
      variations: true,
    },
    themes: {
      light: {
        primary: '#1976d2',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        amber: '#FFC107',
        // grey: '#FFc',
      },
      /*
      dark: {
        primary: '#1976d2',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        amber: '#9e7701',
        grey: '#aa1',
      },
      */
    },
  },

  lang: {
    locales: { pt },
    current: 'pt',
  },
})
