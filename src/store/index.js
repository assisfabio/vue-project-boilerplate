import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: 0,
  },
  mutations: {
    loadingIncrement: state => state.loading++,
    loadingDecrement: state => state.loading--,
  },
  actions: {},
  modules: {},
})
