const message = require('./message');
const response = require('./response');

module.exports = {
  message,
  response
};