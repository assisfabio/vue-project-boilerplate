module.exports = [
  {
    id: 1,
    name: 'Loja da Manu',
    address: 'Av. Ataulfo de Paiva 123',
    neighborhood: 'Leblon',
    city: 'Rio de Janeiro',
    status: 1,
    document: {
      type: 1,
      number: '12.345.678/0001-00'
    },
    partners: [
      {
        name: 'Manu',
        document: '123.456.789-10'
      }
    ]
  },
  {
    id: 2,
    name: 'Padaria do Seu José',
    address: 'Av. Rio Branco 123',
    neighborhood: 'Centro',
    city: 'Rio de Janeiro',
    status: 2,
    document: {
      type: 1,
      number: '23.456.789/0001-21'
    },
    partners: [
      {
        name: 'José',
        document: '234.567.891-01'
      }
    ]
  },
  {
    id: 3,
    name: 'Martelinho de Ouro da Carla',
    address: 'Rua da Passagem 13',
    neighborhood: 'Botafogo',
    city: 'Rio de Janeiro',
    status: 4,
    document: {
      type: 1,
      number: '34.567.890/0001-41'
    },
    partners: [
      {
        name: 'Carla Silva',
        document: '345.678.901-23'
      },
      {
        name: 'Maria Silva',
        document: '456.789.012-34'
      }
    ]
  },
  {
    id: 4,
    name: 'Brigadeiro da Vó Zelda',
    address: 'Rua Frederico Méier 10',
    neighborhood: 'Méier',
    city: 'Rio de Janeiro',
    status: 3,
    document: {
      type: 1,
      number: '45.678.901/0001-31'
    },
    partners: [
      {
        name: 'Zelda',
        document: '567.890.123-45'
      }
    ]
  }
];