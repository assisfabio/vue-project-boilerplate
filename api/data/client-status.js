module.exports = [
  {
    id: 1,
    description: 'Aprovado'
  },
  {
    id: 2,
    description: 'Bloqueado'
  },
  {
    id: 3,
    description: 'Cancelado'
  },
  {
    id: 4,
    description: 'Descredenciado'
  }
];