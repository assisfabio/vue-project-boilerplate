module.exports = {
  build: {
    current: 1578597305, // build version Timestamp or build number
    min: 1577906105,
    deprecated: {
      status: false,
      action: 'block',
    },
  },
  version: '0.0.1-featured',
  license: {
    id: 20200823191755,
  },
  nav: [
    {
      icon: 'monitor-dashboard',
      title: 'Dashboard',
      text: 'Relatorios e Ferramentas',
      link: '/',
    },
    {
      icon: 'calendar',
      title: 'Agenda',
      link: 'schedule',
    },
    /*
{ divider: true },
{ heading: 'Atalhos', action: 'openAction', text: 'Editar' },
{
  icon: 'account-plus',
  title: 'Add novo cliente',
  link: 'Home',
},
*/
    { divider: true },
    { title: 'Clientes', icon: 'account-heart', link: 'clientes' },
    {
      title: 'Licenciados',
      icon: 'folder-account',
      link: 'licenciados',
    },
    {
      title: 'Usuários',
      icon: 'clipboard-account',
      link: 'usuarios',
    },
    { title: 'Sobre', icon: 'help-box', link: 'about' },
    { title: 'Pagina Dinâmica 3', icon: 'sync', link: 'dynamic-three' },
    { title: 'Pagina Dinâmica 4', icon: 'sync', link: 'dynamic-four' },
    {
      title: 'Preferências',
      icon: 'application-cog',
      link: 'settings',
    },
  ],
  routes: [
    {
      name: 'DynamicThree',
      path: '/dynamic-three',
      src:
        'https://f2p.com.br/vue-libs/rsv-teste/RsvTesteComponent.0e3563d8468df3a60d9a.umd.min.js',
      style:
        'https://f2p.com.br/vue-libs/rsv-teste/RsvTesteComponent.0e3563d8468df3a60d9a.css',
    },
    {
      name: 'DynamicFour',
      path: '/dynamic-four',
      src:
        'https://f2p.com.br/vue-libs/rsv-teste/RsvTesteComponent.0e3563d8468df3a60d9a.umd.min.js',
      style:
        'https://f2p.com.br/vue-libs/rsv-teste/RsvTesteComponent.0e3563d8468df3a60d9a.css',
    },
  ],
}
