module.exports = {
  build: {
    current: 1578597305, // build version Timestamp or build number
    min: 1577906105,
    deprecated: {
      status: false,
      action: 'block',
    },
  },
  api: {
    name: 'nome da api [app, core, whatever]',
    version: '1.0.2',
    host: 'https://domain.com/api/',
  },
  schema: 'SQL STRUCTURE',
}
