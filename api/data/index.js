const auth = require('./auth')
const cardBrands = require('./card-brands')
const clients = require('./clients')
const clientStatus = require('./client-status')
const components = require('./components')
const license = require('./license')
const status = require('./status')
const transaction = require('./transaction')
const transactionStatus = require('./transaction-status')
const users = require('./users')

module.exports = {
  auth,
  cardBrands,
  clients,
  clientStatus,
  components,
  license,
  status,
  transaction,
  transactionStatus,
  users,
}
