const manifest = require('./public/manifest.json')
const configureAPI = require('./api')

module.exports = {
  transpileDependencies: ['vuetify'],
  publicPath: process.env.PUBLIC_PATH || '/',
  devServer: {
    before: configureAPI,
  },
  productionSourceMap: false,
  pwa: {
    name: manifest.name,
    themeColor: manifest.theme_color,
    msTileColor: manifest.background_color,
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: manifest.background_color,
    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'src/service-worker.js',
      // ...other Workbox options...
      exclude: ['firebase-messaging-sw.js', '.htaccess'],
    },
  },
}
