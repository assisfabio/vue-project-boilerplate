importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-messaging.js')

var firebaseConfig = {
  apiKey: '%REPLACE%',
  authDomain: 'boilerplate.firebaseapp.com',
  databaseURL: 'https://boilerplate.firebaseio.com',
  projectId: 'boilerplate',
  storageBucket: 'boilerplate.appspot.com',
  messagingSenderId: '%REPLACE%',
  appId: '%REPLACE%',
  measurementId: '%REPLACE%',
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

/**
 * Messaging
 */
const messaging = firebase.messaging()

// [START background_handler]
messaging.setBackgroundMessageHandler(function (payload) {
  console.log(
    '[firebase-messaging-sw.js] Received background message ',
    payload
  )
  // Customize notification here
  const notificationTitle = 'Background Message Title'
  const notificationOptions = {
    body: 'Background Message body.',
    icon: 'https://domain.com/icon.png',
  }

  return self.registration.showNotification(
    notificationTitle,
    notificationOptions
  )
})
// [END background_handler]
